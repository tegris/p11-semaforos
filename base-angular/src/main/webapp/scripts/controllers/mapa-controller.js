angular.module('base').controller('MapaController', function($scope, $state, MockService) {

    //Mock Data
    var semaforos = MockService.mock().getSemaforos();

    var map = L.map('map', {
        center: [-23.550016, -46.633743],
        zoom: 13
    });

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);

    var iconeSemaforo = L.icon({
        iconUrl: 'images/semaforo.svg',
        iconSize: [30, 30]
//        iconAnchor: [15, 15]
//        popupAnchor: [],
    });

    function pupularSemaforos() {
        var markersLayer = new L.LayerGroup();	//layer contain searched elements
    	map.addLayer(markersLayer);

        for (var i=0; i<semaforos.length; i++) {
            var marker = L.marker([semaforos[i].latitude, semaforos[i].longitude], {icon: iconeSemaforo, title: semaforos[i].local}).bindPopup(criaPopup(semaforos[i]));
            markersLayer.addLayer(marker);
        }
        return markersLayer;

    }

    function criaPopup(semaforo) {
       var container = L.DomUtil.create('div', '');
       createLabel(semaforo.local, container);
//       createLink('Novo plano', '#/base/cadastro','', container);
//       createLink('Editar plano', '#/base/cadastro/' + 1, 'pull-right', container);
       createLink('Detalhes', '#/base/cadastro/' + semaforo.id_local + '/detalhe', '', container);

        return container;
    }

    var markersLayer = pupularSemaforos();

    var controlSearch = new L.Control.Search({
        layer: markersLayer,
        initial: false,
        zoom: 18,
        position: 'bottomleft',
        textPlaceholder: 'Busque por endereço ou código do computador',
        collapsed: false,
        textErr: 'nenhum registro encontrado',
        textCancel: 'Cancelar'
    });

    map.addControl( controlSearch );

    controlSearch.on('search_locationfound', function(e) {
        e.layer.openPopup();
    });


    function createLink(label, href, estilo, container) {
        var link = L.DomUtil.create('a', estilo, container);
        link.href = href;
        link.innerHTML = label;
        return link;;
    }

    function createLabel(texto, container) {
        var label = L.DomUtil.create('span', '', container);
        label.innerHTML = texto + "<br/>";
        return label;;
    }

});
