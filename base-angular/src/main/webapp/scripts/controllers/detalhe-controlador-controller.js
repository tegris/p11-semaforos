angular.module('base').controller('DetalheControladorController',  function ($scope, $state, $filter, $stateParams, MockService) {
    var semaforos = MockService.mock().getSemaforos();

    $scope.semaforo = $filter('filter')(semaforos, {'id_local' : $stateParams.id})[0];

    $scope.controlador = {
        id: 1234,
        nome: "TH-400",
        planos: [
            { id: 1, nome: "Plano 1"},
            { id: 2, nome: "Plano 2"},
            { id: 3, nome: "Plano 3"},
            { id: 4, nome: "Plano 4"},
            { id: 5, nome: "Plano 5"}
        ]
    };

    $scope.tabela = [];

    for (var i=0; i<10; i++) {

        plano = {
            troca: i,
            plano: "P" + i,
            periodo: "seg/sab"
        }

        $scope.tabela.push(plano);
    }

    $scope.editar = function(_id) {
        $state.go("base.edit", {id: _id});
    }

    var iconeSemaforo = L.icon({
        iconUrl: 'images/semaforo.svg',
        iconSize: [30, 30]
//        iconAnchor: [15, 15]
//        popupAnchor: [],
    });

    var map = L.map('map', {
        center: [$scope.semaforo.latitude, $scope.semaforo.longitude],
        zoom: 18,
        dragging: false,
        touchZoom: false,
        scrollWheelZoom: false,
        doubleClickZoom: false,
        boxZoom: false,
        tap: false,
        trackResize: false,
        closePopupOnClick: false,
        keyboard: false,
        zoomControl: false
    });

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(map);

    var marker = L.marker([$scope.semaforo.latitude, $scope.semaforo.longitude], {icon: iconeSemaforo, title: $scope.semaforo.local}).bindPopup("<p>" + $scope.semaforo.local + "<br/><b>" + $scope.semaforo.distrito + "</b></p>").addTo(map);
    marker.openPopup();

});
