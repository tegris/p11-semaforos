angular.module('base').controller('PlanoControladorController',  function ($scope, Endpoints, RestService) {
	
	var getNewEstagio = function(index) {
		var estagio = new Object();
		estagio.id = index;
		estagio.indispensavel = false;
		estagio.variavel = false;
		estagio.tempoDeVerde= 0;
		estagio.tempoDeVerdeMinimo= 0;
		estagio.tempoDeVerdeMaximo= 0;
		estagio.tempoDeExtensaoDeVerde= 0;
		estagio.tempoDeAmarelo= 0;
		estagio.tempoDeVermelhoIntermitente= 0;
		estagio.tempoDeVermelhoDeLimpeza= 0;
		return estagio;
	};
	
	var getControlador = function() {
		var controlador = new Object();
		controlador.planos = [];
		$scope.plano = {};
		$scope.plano.aneis = [];
		controlador.planos.push($scope.plano);
		for (var int = 1; int < 9; int++) {
			var anel = new Object();
			anel.id = int;
			var configuracao = new Object();
			configuracao.estagiosAssociadosBotoeira = [];
			for (var int2 = 0; int2 < 4; int2++) {
				var botoeira = new Object();
				botoeira.id = int2 +1;
				botoeira.estagios = [];
				configuracao.estagiosAssociadosBotoeira.push(botoeira)
			}
			configuracao.estagiosAssociadosDeteccaoVeiculos = [];
			for (var int3 = 0; int3 < 8; int3++) {
				var detetor = new Object();
				detetor.id = int3 +1;
				detetor.estagios = [];
				configuracao.estagiosAssociadosDeteccaoVeiculos.push(detetor)
			}
			anel.configuracao = configuracao;
			anel.estagios = []
			anel.estagios.push(getNewEstagio(anel.estagios.length+1));
			$scope.plano.aneis.push(anel);
		}
		return controlador;
	}
	
	var init = function() {
		$scope.controlador = getControlador();
		$scope.anel = $scope.plano.aneis[0];
		$scope.anelLabel = String.fromCharCode(64+$scope.anel.id);
		ativar($scope.anel.id, true);
	}
	init();

	$scope.selectRing = function(pos) {
		ativar(parseInt($scope.anel.id), false);
		ativar((pos+1), true);

		$scope.anel = $scope.plano.aneis[pos];
		$scope.anelLabel = String.fromCharCode(64+$scope.anel.id);		
	}

	$scope.addEstagio = function() {
		if($scope.anel.estagios.length < 16) {
			$scope.anel.estagios.push(getNewEstagio($scope.anel.estagios.length+1));
		}
	};

	 function ativar(id, flag) {
	        if (flag) {
	        	angular.element('#ring-id-'+id).addClass('botaoAtivo');
	        } else {
	        	angular.element('#ring-id-'+id).removeClass('botaoAtivo');
	        }
	};
	 
	$scope.salvar = function(){
		RestService.requests(Endpoints.Base.Controlador.xml).post($scope.controlador).$promise.then(function(result){
			var blob= new Blob([result.xml], {type:'application/xml'});
		    var link=document.createElement('a');
		    link.href=window.URL.createObjectURL(blob);
		    link.download="controlador.xml";
		    link.click();
		}, function(promise){
			alertify.error("Não foi possível baixar o XML de controladores.");
		});
	};

});
