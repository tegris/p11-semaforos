angular.module('base').controller('TesteController',  function ($scope, RestService, Endpoints) {
	$scope.color = "#FF0000";
	
	RestService.requests(Endpoints.Base.Semaforo.listagem).get().$promise.then(function(result){
	    $scope.semaforos = result.objects;
	    console.log($scope.semaforos);
	}, function(promise){
		alertify.error("Não foi possível carregar os semáforos.");
	});
	
	RestService.requests(Endpoints.Base.Controlador.listagem).get().$promise.then(function(result){
		$scope.controladores = result.objects;
		console.log($scope.controladores);
	}, function(promise){
		alertify.error("Não foi possível carregar os controladores.");
	});
	
	$scope.xmlSemaforo = function(){
		RestService.requests(Endpoints.Base.Semaforo.xml).post($scope.semaforos[7]).$promise.then(function(result){
			var blob= new Blob([result.xml], {type:'application/xml'});
		    var link=document.createElement('a');
		    link.href=window.URL.createObjectURL(blob);
		    link.download="semaforo.xml";
		    link.click();
		}, function(promise){
			alertify.error("Não foi possível baixar o XML de semáforos.");
		});
	};
	
	$scope.xmlControlador = function(){
		RestService.requests(Endpoints.Base.Controlador.xml).post($scope.controladores[7]).$promise.then(function(result){
			var blob= new Blob([result.xml], {type:'application/xml'});
		    var link=document.createElement('a');
		    link.href=window.URL.createObjectURL(blob);
		    link.download="controlador.xml";
		    link.click();
		}, function(promise){
			alertify.error("Não foi possível baixar o XML de controladores.");
		});
	};
	
});
