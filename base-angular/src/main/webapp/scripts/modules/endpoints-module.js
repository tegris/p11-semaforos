angular.module('base.endpoints', []).constant('Endpoints', {
	Base : {		
		enums : '/base-resources/enum/:enum_desejada/',
		Semaforo: {
			listagem : '/base-resources/semaforo/',
			xml: '/base-resources/semaforo/xml'
		},
		Controlador: {
			listagem : '/base-resources/controlador/',
			xml: '/base-resources/controlador/xml'
		}
			
	},
	
	Services : {
	}
});
