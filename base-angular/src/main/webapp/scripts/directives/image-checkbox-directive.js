angular.module('base').directive('imageCheckbox', function (baseConstants) {
    return {
      restrict: 'E',
      scope: {object : '=object', key : '=key', checkedImage : '=checkedImage', uncheckedImage : '=uncheckedImage'},
      templateUrl: baseConstants.templates.components.imageCheckbox,
      link: function(scope, element, attrs){
    	  scope.switchValue = function(){
    		  scope.object[scope.key] = !scope.object[scope.key]; 
    	  };
      }
  	};
});
