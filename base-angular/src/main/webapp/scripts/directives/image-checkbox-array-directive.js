angular.module('base').directive('imageCheckboxArray', function (baseConstants) {
    return {
      restrict: 'E',
      scope: {array : '=array', key : '=key', checkedImage : '=checkedImage', uncheckedImage : '=uncheckedImage'},
      templateUrl: baseConstants.templates.components.imageCheckboxArray,
      link: function(scope, element, attrs){
    	  scope.switchValue = function(){
    		  var position = scope.array.indexOf(scope.key);
    		  if(position === -1){
    			  scope.array.push(scope.key);
    		  }else{
    			  scope.array.splice(position, 1);
    		  }
    	  };
      }
  	};
});
