angular.module('base').directive('toggleButton', function (baseConstants) {
    return {
      restrict: 'E',
      scope: {color:'=color', text:'=text', 'default': '=default'},
      templateUrl: baseConstants.templates.components.toggleButton,
      link: function(scope, element, attrs){
    	  scope.toggled = scope['default'];
    	  
    	  scope.toggle = function(){
    		  scope.toggled = !scope.toggled; 
    	  };
      }
  	};
});
