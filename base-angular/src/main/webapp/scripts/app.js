var baseApp = angular.module('base',
  ['base.endpoints',
   'ui.router',
   'ngResource',
   'ui.bootstrap'
  ]).constant('baseConstants',{
		templates: {			
			cadastroControlador: 'views/partials/Semaforos.html',
			detalheControlador: 'views/partials/detalhes.html',
            mapa: 'views/partials/mapa.html',
			components: {
				imageCheckbox : 'views/partials/components/image-checkbox.html',
				imageCheckboxArray : 'views/partials/components/image-checkbox-array.html',
				toggleButton : 'views/partials/components/toggle-button.html'
			},			
			modals: {
			}
		},
		urls: {
            base: "/base",
			cadastro: "/cadastro",
            mapa: "/mapa"
		}
	}).config(function($stateProvider, $urlRouterProvider, baseConstants) {

		// States
		$stateProvider
            .state('base', {
                name: 'base',
                abstract: true,
                url: baseConstants.urls.base,
                template: "<div ui-view></div>",
                data: {
                    requireLogin: true
                }
            }).state('base.init', {
                name: 'base.init',
                url: baseConstants.urls.cadastro,
                templateUrl: baseConstants.templates.cadastroControlador,
                requireLogin: false
            }).state('base.edit', {
                    name: 'base.edit',
                    url: baseConstants.urls.cadastro + "/:id",
                    templateUrl: baseConstants.templates.cadastroControlador,
                    requireLogin: false
            }).state('base.detalhe', {
                name: 'base.detalhe',
                url: baseConstants.urls.cadastro + "/:id/detalhe",
                templateUrl: baseConstants.templates.detalheControlador,
                requireLogin: false,
                controller: 'DetalheControladorController'
            }).state('base.mapa', {
                name: 'base.mapa',
                    url: baseConstants.urls.mapa,
                    templateUrl: baseConstants.templates.mapa,
                    requireLogin: false,
                    controller: 'MapaController'
            });
    });

baseApp.run(function ($rootScope, $state) {
	$state.go("base.init");
	$rootScope.is = function(name) {
		return $state.is(name);
	}
	$rootScope.includes = function(name) {
		return $state.includes(name);
	}
	
});