angular.module('base').service("DsdArrayUtils", function() {
	
	this.findIndex = function(array, object, comparator) {
		if(comparator){
			for(i = 0; i < array.length; i++) {
				if(comparator(array[i], object)) {
					return i;
				}
			}
		}else{
			for(i = 0; i < array.length; i++) {
				if(array[i].id === object.id) {
					return i;
				}
			}
		}
		return -1;
	};
	
	this.remove = function(array, object, comparator) {
		var index = this.findIndex(array, object, comparator);
		array.splice(index, 1);
	};
	
});