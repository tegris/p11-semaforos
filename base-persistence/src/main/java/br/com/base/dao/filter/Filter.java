package br.com.base.dao.filter;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

/**
 * Interface para filtros em buscas no banco.
 * @author wilerson.oliveira
 */
public interface Filter {

    /**
     * Método que constrói o Predicate para o CriteriaBuilder.
     * @return um predicate de acordo com os parâmetros configurados
     */
    Predicate buildPredicate();

    /**
     * Seta o CriteriaBuilder nesta instância
     * @param cb o CriteriaBuilder
     * @return esta instância, para fazer encadeamento de chamadas
     */
    Filter setCriteriaBuilder(final CriteriaBuilder cb);

    /**
     * Seta o caminho do campo da comparação.
     * @param path o caminho para o campo
     * @return esta instância, para fazer encadeamento de chamadas
     */
    Filter setPath(final Path path);

    /**
     * Seta o valor devidamente convertido para ser enviado ao CriteriaBuilder
     * @param value o valor convertido
     * @return esta instância, para fazer encadeamento de chamadas
     */
    Filter setConvertedValue(final Object value);
}
