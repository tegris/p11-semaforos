package br.com.base.dao.poc;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import br.org.dsd.common.jpa.model.LongModel;

@Entity
@Table(name = "semaforo")
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement(name = "semaforo")
public class Semaforo extends LongModel {

    /**
     * Serial UID.
     */
    private static final long serialVersionUID = -5452039335298202599L;

    @Id
    @SequenceGenerator(allocationSize = 0, name = "semaforo_seq", sequenceName = "semaforo_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "semaforo_seq")
    private Long id;

    @Column(name = "id_local")
    private Long idLocal;

    private String local;

    private String distrito;

    private String area;

    private String departamento;

    private String controlador;

    private String dcs;

    private String latitude;

    private String longitude;

    @Override
    public Long getId() {
        return this.id;
    }

    public Long getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(Long idLocal) {
        this.idLocal = idLocal;
    }

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getControlador() {
        return controlador;
    }

    public void setControlador(String controlador) {
        this.controlador = controlador;
    }

    public String getDcs() {
        return dcs;
    }

    public void setDcs(String dcs) {
        this.dcs = dcs;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
