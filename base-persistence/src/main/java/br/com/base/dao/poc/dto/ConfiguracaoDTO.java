package br.com.base.dao.poc.dto;

import java.util.Set;
import br.com.base.dao.poc.SequencialEstagio;
import br.com.base.dao.poc.interfaces.ConfiguracaoInterface;


public class ConfiguracaoDTO implements ConfiguracaoInterface {

    /**
     * Serial UID.
     */
    private static final long serialVersionUID = -6576378709448385638L;

    private Set<BotoeiraDTO> estagiosAssociadosBotoeira;

    private Set<DetetorVeiculoDTO> estagiosAssociadosDeteccaoVeiculos;

    private SequencialEstagio sequenciaDeEstagio;

    public Set<BotoeiraDTO> getEstagiosAssociadosBotoeira() {
        return estagiosAssociadosBotoeira;
    }

    public void setEstagiosAssociadosBotoeira(Set<BotoeiraDTO> estagiosAssociadosBotoeira) {
        this.estagiosAssociadosBotoeira = estagiosAssociadosBotoeira;
    }

    public Set<DetetorVeiculoDTO> getEstagiosAssociadosDeteccaoVeiculos() {
        return estagiosAssociadosDeteccaoVeiculos;
    }

    public void setEstagiosAssociadosDeteccaoVeiculos(Set<DetetorVeiculoDTO> estagiosAssociadosDeteccaoVeiculos) {
        this.estagiosAssociadosDeteccaoVeiculos = estagiosAssociadosDeteccaoVeiculos;
    }

    public SequencialEstagio getSequenciaDeEstagio() {
        return sequenciaDeEstagio;
    }

    public void setSequenciaDeEstagio(SequencialEstagio sequenciaDeEstagio) {
        this.sequenciaDeEstagio = sequenciaDeEstagio;
    }


}
