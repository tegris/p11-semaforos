package br.com.base.dao.poc.parse;

import java.io.ByteArrayOutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import br.com.base.dao.poc.Controlador;

public class ControladorParser {
	
	public static ByteArrayOutputStream marshallToByteArray(Controlador controlador) {
		try {
			JAXBContext jc = JAXBContext.newInstance(Controlador.class);
			Marshaller u = jc.createMarshaller();
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			u.marshal(controlador, os);
			return os;
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return null;
	}

}
