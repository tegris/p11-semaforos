package br.com.base.dao.poc;

import br.com.base.dao.BaseDAO;

public class ControladorDAO extends BaseDAO<Controlador> {

    @Override
    protected Class<Controlador> getEntityType() {
        return Controlador.class;
    }

}
