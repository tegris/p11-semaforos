package br.com.base.dao.poc;

import java.util.Set;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import br.com.base.dao.poc.interfaces.PlanoInterface;
import br.org.dsd.common.jpa.model.LongModel;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Plano extends LongModel implements PlanoInterface {

    /**
     * Serial UID.
     */
    private static final long serialVersionUID = -7746242609241009155L;

    private Long id;

    private Integer tempoDeCiclo;

    private Integer tempoDeCicloMaximo;

    private Set<Anel> aneis;

    @Override
    @XmlAttribute(name = "id")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @XmlAttribute(name = "tempoDeCiclo")
    public Integer getTempoDeCiclo() {
        return tempoDeCiclo;
    }

    public void setTempoDeCiclo(Integer tempoDeCiclo) {
        this.tempoDeCiclo = tempoDeCiclo;
    }

    @XmlAttribute(name = "tempoDeCicloMaximo")
    public Integer getTempoDeCicloMaximo() {
        return tempoDeCicloMaximo;
    }

    public void setTempoDeCicloMaximo(Integer tempoDeCicloMaximo) {
        this.tempoDeCicloMaximo = tempoDeCicloMaximo;
    }

    @XmlElementWrapper
    @XmlElements({@XmlElement(name = "anel")})
    public Set<Anel> getAneis() {
        return aneis;
    }

    public void setAneis(Set<Anel> aneis) {
        this.aneis = aneis;
    }

}
