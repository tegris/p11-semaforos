package br.com.base.dao.poc;

import javax.xml.bind.annotation.XmlAttribute;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import br.org.dsd.common.jpa.model.LongModel;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Estagio extends LongModel {

    /**
     * Serial UID.
     */
    private static final long serialVersionUID = -6201952550558395616L;

    private Long id;

    private boolean indispensavel;

    private boolean variavel;

    private Integer tempoDeVerde;

    private Integer tempoDeVerdeMinimo;

    private Integer tempoDeVerdeMaximo;

    private Integer tempoDeExtensaoDeVerde;

    private Integer tempoDeAmarelo;

    private Integer tempoDeVermelhoIntermitente;

    private Integer tempoDeVermelhoDeLimpeza;

    @Override
    @XmlAttribute
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @XmlAttribute
    public boolean isIndispensavel() {
        return indispensavel;
    }

    public void setIndispensavel(boolean indispensavel) {
        this.indispensavel = indispensavel;
    }

    @XmlAttribute
    public boolean isVariavel() {
        return variavel;
    }

    public void setVariavel(boolean variavel) {
        this.variavel = variavel;
    }

    @XmlAttribute
    public Integer getTempoDeVerde() {
        return tempoDeVerde;
    }

    public void setTempoDeVerde(Integer tempoDeVerde) {
        this.tempoDeVerde = tempoDeVerde;
    }

    @XmlAttribute
    public Integer getTempoDeVerdeMinimo() {
        return tempoDeVerdeMinimo;
    }

    public void setTempoDeVerdeMinimo(Integer tempoDeVerdeMinimo) {
        this.tempoDeVerdeMinimo = tempoDeVerdeMinimo;
    }

    @XmlAttribute
    public Integer getTempoDeVerdeMaximo() {
        return tempoDeVerdeMaximo;
    }

    public void setTempoDeVerdeMaximo(Integer tempoDeVerdeMaximo) {
        this.tempoDeVerdeMaximo = tempoDeVerdeMaximo;
    }

    @XmlAttribute
    public Integer getTempoDeExtensaoDeVerde() {
        return tempoDeExtensaoDeVerde;
    }

    public void setTempoDeExtensaoDeVerde(Integer tempoDeExtensaoDeVerde) {
        this.tempoDeExtensaoDeVerde = tempoDeExtensaoDeVerde;
    }

    @XmlAttribute
    public Integer getTempoDeAmarelo() {
        return tempoDeAmarelo;
    }

    public void setTempoDeAmarelo(Integer tempoDeAmarelo) {
        this.tempoDeAmarelo = tempoDeAmarelo;
    }

    @XmlAttribute
    public Integer getTempoDeVermelhoIntermitente() {
        return tempoDeVermelhoIntermitente;
    }

    public void setTempoDeVermelhoIntermitente(Integer tempoDeVermelhoIntermitente) {
        this.tempoDeVermelhoIntermitente = tempoDeVermelhoIntermitente;
    }

    @XmlAttribute
    public Integer getTempoDeVermelhoDeLimpeza() {
        return tempoDeVermelhoDeLimpeza;
    }

    public void setTempoDeVermelhoDeLimpeza(Integer tempoDeVermelhoDeLimpeza) {
        this.tempoDeVermelhoDeLimpeza = tempoDeVermelhoDeLimpeza;
    }

}
