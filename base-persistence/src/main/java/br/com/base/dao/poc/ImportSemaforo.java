package br.com.base.dao.poc;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class ImportSemaforo {

    public static void main(String[] args) {
        try {
            final List<String> lines = Files.readAllLines(Paths.get("C:\\Users\\Felipe\\Desktop\\csv.csv"));
            for (String line : lines) {
                final String[] semaforoSplit = line.split(";");
                semaforoSplit[semaforoSplit.length - 1] = semaforoSplit[semaforoSplit.length - 1].replace(",", ".");
                semaforoSplit[semaforoSplit.length - 2] = semaforoSplit[semaforoSplit.length - 2].replace(",", ".");
                String baseQuery = "INSERT INTO semaforo(id, area, controlador, dcs, departamento, distrito, local, id_local, latitude, longitude) VALUES (nextval(\'semaforo_seq\'), \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', \'%s\', %s, \'%s\', \'%s\');";
                System.out.println(String.format(baseQuery, (Object[]) semaforoSplit));
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
