package br.com.base.dao.poc.dto;

import java.util.Date;
import java.util.Set;
import br.com.base.dao.poc.interfaces.ControladorInterface;

public class ControladorDTO implements ControladorInterface {

    /**
     * Serial UID.
     */
    private static final long serialVersionUID = 8325772483738479089L;

    private Long id;

    private String enderecoIp;

    private Date timestamp;

    private Long idRequisicao;

    private Set<PlanoDTO> planos;

    /*
     * (non-Javadoc)
     * 
     * @see br.com.base.dao.poc.dto.ControladorInterface#getId()
     */
    @Override
    public Long getId() {
        return id;
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.base.dao.poc.dto.ControladorInterface#setId(java.lang.Long)
     */
    public void setId(Long id) {
        this.id = id;
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.base.dao.poc.dto.ControladorInterface#getEnderecoIp()
     */
    @Override
    public String getEnderecoIp() {
        return enderecoIp;
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.base.dao.poc.dto.ControladorInterface#setEnderecoIp(java.lang. String)
     */
    public void setEnderecoIp(String enderecoIp) {
        this.enderecoIp = enderecoIp;
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.base.dao.poc.dto.ControladorInterface#getTimestamp()
     */
    @Override
    public Date getTimestamp() {
        return timestamp;
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.base.dao.poc.dto.ControladorInterface#setTimestamp(java.util.Date)
     */
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.base.dao.poc.dto.ControladorInterface#getIdRequisicao()
     */
    @Override
    public Long getIdRequisicao() {
        return idRequisicao;
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.base.dao.poc.dto.ControladorInterface#setIdRequisicao(java.lang .Long)
     */
    public void setIdRequisicao(Long idRequisicao) {
        this.idRequisicao = idRequisicao;
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.base.dao.poc.dto.ControladorInterface#getPlanos()
     */
    @Override
    public Set<PlanoDTO> getPlanos() {
        return planos;
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.base.dao.poc.dto.ControladorInterface#setPlanos(java.util.Set)
     */
    public void setPlanos(Set<PlanoDTO> planos) {
        this.planos = planos;
    }

}
