package br.com.base.dao.poc;

import java.util.Set;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import br.com.base.dao.poc.interfaces.ConfiguracaoInterface;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Configuracao implements ConfiguracaoInterface {

    /**
     * Serial UID.
     */
    private static final long serialVersionUID = -7715337691917055400L;

    private Set<Botoeira> estagiosAssociadosBotoeira;

    private Set<DetetorVeiculo> estagiosAssociadosDeteccaoVeiculos;

    private SequencialEstagio sequenciaDeEstagio;

    @XmlElementWrapper
    @XmlElements({@XmlElement(name = "botoeira")})
    public Set<Botoeira> getEstagiosAssociadosBotoeira() {
        return estagiosAssociadosBotoeira;
    }

    public void setEstagiosAssociadosBotoeira(Set<Botoeira> estagiosAssociadosBotoeira) {
        this.estagiosAssociadosBotoeira = estagiosAssociadosBotoeira;
    }

    @XmlElementWrapper
    @XmlElements({@XmlElement(name = "detetorVeiculo")})
    public Set<DetetorVeiculo> getEstagiosAssociadosDeteccaoVeiculos() {
        return estagiosAssociadosDeteccaoVeiculos;
    }

    public void setEstagiosAssociadosDeteccaoVeiculos(Set<DetetorVeiculo> estagiosAssociadosDeteccaoVeiculos) {
        this.estagiosAssociadosDeteccaoVeiculos = estagiosAssociadosDeteccaoVeiculos;
    }

    public SequencialEstagio getSequenciaDeEstagio() {
        return sequenciaDeEstagio;
    }

    public void setSequenciaDeEstagio(SequencialEstagio sequenciaDeEstagio) {
        this.sequenciaDeEstagio = sequenciaDeEstagio;
    }

}
