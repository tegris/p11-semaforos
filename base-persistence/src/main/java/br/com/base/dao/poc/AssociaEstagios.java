package br.com.base.dao.poc;

import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import br.com.base.dao.poc.interfaces.AssociaEstagiosInterface;


public abstract class AssociaEstagios implements AssociaEstagiosInterface {
    
    /**
     * Serial UID.
     */
    private static final long serialVersionUID = 8749990538664874640L;

    private Long id;

    private Set<Long> estagios;
    
    private String estagiosXML;
    
    /* (non-Javadoc)
     * @see br.com.base.dao.poc.AssociaEstagiosInterface#getId()
     */
    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /* (non-Javadoc)
     * @see br.com.base.dao.poc.AssociaEstagiosInterface#getEstagios()
     */
    @Override
    public Set<Long> getEstagios() {
        return estagios;
    }

    public void setEstagios(Set<Long> estagios) {
        this.estagios = estagios;
    }

    public String getEstagiosXML() {
        if(estagiosXML != null) {
            return estagiosXML;
        }
        if(CollectionUtils.isNotEmpty(estagios)) {
            estagiosXML =  String.join(",", estagios.stream().map(e -> e.toString()).collect(Collectors.toList()));
            estagios = null;
        } else {
            estagiosXML = "";
        }
        return estagiosXML;
    }

}
