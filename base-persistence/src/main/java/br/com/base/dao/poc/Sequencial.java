package br.com.base.dao.poc;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAttribute;

public class Sequencial implements Serializable {
	
	/**
     * Serial UID.
     */
    private static final long serialVersionUID = -8550178569566375990L;
    
    private Long estagio;
    
    public Sequencial() {
        
    }
    
    public Sequencial(Long estagio) {
        this.estagio = estagio;
    }

	@XmlAttribute
	public Long getEstagio() {
		return estagio;
	}

	public void setEstagio(Long estagio) {
		this.estagio = estagio;
	}

}
