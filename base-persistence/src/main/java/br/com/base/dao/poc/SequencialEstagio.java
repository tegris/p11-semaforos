package br.com.base.dao.poc;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SequencialEstagio {

    private Sequencial primeiroEstagioDaSequencia;

    private Sequencial segundoEstagioDaSequencia;

    private Sequencial terceiroEstagioDaSequencia;

    private Sequencial quartoEstagioDaSequencia;

    private Sequencial quintoEstagioDaSequencia;

    private Sequencial sextoEstagioDaSequencia;

    private Sequencial setimoEstagioDaSequencia;

    private Sequencial oitavoEstagioDaSequencia;

    private Sequencial nonoEstagioDaSequencia;

    private Sequencial decimoEstagioDaSequencia;

    private Sequencial decimoPrimeiroEstagioDaSequencia;

    private Sequencial decimoSegundoEstagioDaSequencia;
    
    private Sequencial decimoTerceiroEstagioDaSequencia;

    private Sequencial decimoQuartoEstagioDaSequencia;

    private Sequencial decimoQuintoEstagioDaSequencia;

    private Sequencial decimoSextoEstagioDaSequencia;
    
    public SequencialEstagio() {
       this.primeiroEstagioDaSequencia = new Sequencial(1L);
       this.segundoEstagioDaSequencia = new Sequencial(2L);
       this.terceiroEstagioDaSequencia = new Sequencial(3L);
       this.quartoEstagioDaSequencia = new Sequencial(4L);
       this.quintoEstagioDaSequencia = new Sequencial(5L);
       this.sextoEstagioDaSequencia = new Sequencial(6L);
       this.setimoEstagioDaSequencia = new Sequencial(7L);
       this.oitavoEstagioDaSequencia = new Sequencial(8L);
       this.nonoEstagioDaSequencia = new Sequencial(9L);
       this.decimoEstagioDaSequencia = new Sequencial(10L);
       this.decimoPrimeiroEstagioDaSequencia = new Sequencial(11L);
       this.decimoSegundoEstagioDaSequencia = new Sequencial(12L);
       this.decimoTerceiroEstagioDaSequencia = new Sequencial(13L);
       this.decimoQuartoEstagioDaSequencia = new Sequencial(14L);
       this.decimoQuintoEstagioDaSequencia = new Sequencial(15L);
       this.decimoSextoEstagioDaSequencia = new Sequencial(16L);
    }

    public Sequencial getPrimeiroEstagioDaSequencia() {
        return primeiroEstagioDaSequencia;
    }

    public void setPrimeiroEstagioDaSequencia(Sequencial primeiroEstagioDaSequencia) {
        this.primeiroEstagioDaSequencia = primeiroEstagioDaSequencia;
    }

    public Sequencial getSegundoEstagioDaSequencia() {
        return segundoEstagioDaSequencia;
    }

    public void setSegundoEstagioDaSequencia(Sequencial segundoEstagioDaSequencia) {
        this.segundoEstagioDaSequencia = segundoEstagioDaSequencia;
    }

    public Sequencial getTerceiroEstagioDaSequencia() {
        return terceiroEstagioDaSequencia;
    }

    public void setTerceiroEstagioDaSequencia(Sequencial terceiroEstagioDaSequencia) {
        this.terceiroEstagioDaSequencia = terceiroEstagioDaSequencia;
    }

    public Sequencial getQuartoEstagioDaSequencia() {
        return quartoEstagioDaSequencia;
    }

    public void setQuartoEstagioDaSequencia(Sequencial quartoEstagioDaSequencia) {
        this.quartoEstagioDaSequencia = quartoEstagioDaSequencia;
    }

    public Sequencial getQuintoEstagioDaSequencia() {
        return quintoEstagioDaSequencia;
    }

    public void setQuintoEstagioDaSequencia(Sequencial quintoEstagioDaSequencia) {
        this.quintoEstagioDaSequencia = quintoEstagioDaSequencia;
    }

    public Sequencial getSextoEstagioDaSequencia() {
        return sextoEstagioDaSequencia;
    }

    public void setSextoEstagioDaSequencia(Sequencial sextoEstagioDaSequencia) {
        this.sextoEstagioDaSequencia = sextoEstagioDaSequencia;
    }

    public Sequencial getSetimoEstagioDaSequencia() {
        return setimoEstagioDaSequencia;
    }

    public void setSetimoEstagioDaSequencia(Sequencial setimoEstagioDaSequencia) {
        this.setimoEstagioDaSequencia = setimoEstagioDaSequencia;
    }

    public Sequencial getOitavoEstagioDaSequencia() {
        return oitavoEstagioDaSequencia;
    }

    public void setOitavoEstagioDaSequencia(Sequencial oitavoEstagioDaSequencia) {
        this.oitavoEstagioDaSequencia = oitavoEstagioDaSequencia;
    }

    public Sequencial getNonoEstagioDaSequencia() {
        return nonoEstagioDaSequencia;
    }

    public void setNonoEstagioDaSequencia(Sequencial nonoEstagioDaSequencia) {
        this.nonoEstagioDaSequencia = nonoEstagioDaSequencia;
    }

    public Sequencial getDecimoEstagioDaSequencia() {
        return decimoEstagioDaSequencia;
    }

    public void setDecimoEstagioDaSequencia(Sequencial decimoEstagioDaSequencia) {
        this.decimoEstagioDaSequencia = decimoEstagioDaSequencia;
    }

    public Sequencial getDecimoPrimeiroEstagioDaSequencia() {
        return decimoPrimeiroEstagioDaSequencia;
    }

    public void setDecimoPrimeiroEstagioDaSequencia(Sequencial decimoPrimeiroEstagioDaSequencia) {
        this.decimoPrimeiroEstagioDaSequencia = decimoPrimeiroEstagioDaSequencia;
    }

    public Sequencial getDecimoSegundoEstagioDaSequencia() {
        return decimoSegundoEstagioDaSequencia;
    }

    public void setDecimoSegundoEstagioDaSequencia(Sequencial decimoSegundoEstagioDaSequencia) {
        this.decimoSegundoEstagioDaSequencia = decimoSegundoEstagioDaSequencia;
    }

    public Sequencial getDecimoQuartoEstagioDaSequencia() {
        return decimoQuartoEstagioDaSequencia;
    }

    public void setDecimoQuartoEstagioDaSequencia(Sequencial decimoQuartoEstagioDaSequencia) {
        this.decimoQuartoEstagioDaSequencia = decimoQuartoEstagioDaSequencia;
    }

    public Sequencial getDecimoQuintoEstagioDaSequencia() {
        return decimoQuintoEstagioDaSequencia;
    }

    public void setDecimoQuintoEstagioDaSequencia(Sequencial decimoQuintoEstagioDaSequencia) {
        this.decimoQuintoEstagioDaSequencia = decimoQuintoEstagioDaSequencia;
    }

    public Sequencial getDecimoSextoEstagioDaSequencia() {
        return decimoSextoEstagioDaSequencia;
    }

    public void setDecimoSextoEstagioDaSequencia(Sequencial decimoSextoEstagioDaSequencia) {
        this.decimoSextoEstagioDaSequencia = decimoSextoEstagioDaSequencia;
    }

    public Sequencial getDecimoTerceiroEstagioDaSequencia() {
        return decimoTerceiroEstagioDaSequencia;
    }

    public void setDecimoTerceiroEstagioDaSequencia(Sequencial decimoTerceiroEstagioDaSequencia) {
        this.decimoTerceiroEstagioDaSequencia = decimoTerceiroEstagioDaSequencia;
    }

}
