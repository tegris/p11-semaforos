package br.com.base.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Objeto que contém informações para paginação de buscas no banco.
 * @author wilerson.oliveira
 */
public class Page<T> implements Serializable {

    /**
     * Serial UID.
     */
    private static final long serialVersionUID = -9112397336888942397L;

    /**
     * Lista de objetos.
     */
    private List<T> objects;

    /**
     * Número de página.
     */
    private Short pageNumber = 1;

    /**
     * Contagem de objetos.
     */
    private Long totalObjects = 0L;

    /**
     * Retorna a lista de objetos.
     * @return a lista de objetos
     */
    public List<T> getObjects() {
        return objects;
    }

    /**
     * Seta a lista de objetos.
     * @param objects a lista de objetos
     */
    public void setObjects(final List<T> objects) {
        this.objects = objects;
    }

    /**
     * Seta a lista de objetos via varargs.
     * @param object a lista de objetos
     */
    public void setObjects(final T... object) {
        this.objects = new ArrayList<T>();
        Collections.addAll(this.objects, object);
    }

    /**
     * Retorna o número da página
     * @return o número da página
     */
    public Short getPageNumber() {
        return pageNumber;
    }

    /**
     * Seta o número da página
     * @param pageNumber o número da página
     */
    public void setPageNumber(final Short pageNumber) {
        this.pageNumber = pageNumber;
    }

    /**
     * Retorna a contagem de objetos.
     * @return a contagem de objetos
     */
    public Long getTotalObjects() {
        return totalObjects;
    }

    /**
     * Seta a contagem de objetos.
     * @param totalObjects a contagem de objetos
     */
    public void setTotalObjects(final Long totalObjects) {
        this.totalObjects = totalObjects;
    }
}
