package br.com.base.dao.poc.interfaces;

import java.io.Serializable;
import java.util.Set;

public interface AssociaEstagiosInterface extends Serializable {

    public Long getId();

    public Set<Long> getEstagios();

}