package br.com.base.dao.poc;

import java.util.Date;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import br.com.base.dao.poc.interfaces.ControladorInterface;
import br.org.dsd.common.jpa.model.LongModel;

@Entity
@Table(name = "controlador")
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement(name = "controlador")
public class Controlador extends LongModel implements ControladorInterface {

    /**
     * Serial UID.
     */
    private static final long serialVersionUID = -5452039335298202599L;

    @Id
    @SequenceGenerator(allocationSize = 0, name = "controlador_seq", sequenceName = "controlador_seq")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "controlador_seq")
    private Long id;

    private String nome;

    @Column(name = "endereco_ip")
    private String enderecoIp;

    @Transient
    private Date timestamp;

    @Transient
    private Long idRequisicao;

    @Transient
    private Set<Plano> planos;

    @Override
    @XmlAttribute
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @XmlAttribute
    public String getEnderecoIp() {
        return enderecoIp;
    }

    public void setEnderecoIp(String enderecoIp) {
        this.enderecoIp = enderecoIp;
    }

    @XmlAttribute
    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @XmlAttribute
    public Long getIdRequisicao() {
        return idRequisicao;
    }

    public void setIdRequisicao(Long idRequisicao) {
        this.idRequisicao = idRequisicao;
    }

    @XmlElementWrapper
    @XmlElements({@XmlElement(name = "plano")})
    public Set<Plano> getPlanos() {
        return planos;
    }

    public void setPlanos(Set<Plano> planos) {
        this.planos = planos;
    }

    @XmlTransient
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
