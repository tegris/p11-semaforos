package br.com.base.dao.poc.interfaces;

import java.io.Serializable;
import java.util.Set;
import br.com.base.dao.poc.SequencialEstagio;

public interface ConfiguracaoInterface extends Serializable {

    public Set<? extends AssociaEstagiosInterface> getEstagiosAssociadosBotoeira();

    public Set<? extends AssociaEstagiosInterface> getEstagiosAssociadosDeteccaoVeiculos();

    public SequencialEstagio getSequenciaDeEstagio();

}