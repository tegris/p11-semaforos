package br.com.base.dao.poc.interfaces;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

public interface ControladorInterface extends Serializable {

    public Long getId();

    public String getEnderecoIp();

    public Date getTimestamp();

    public Long getIdRequisicao();

    public Set<? extends PlanoInterface> getPlanos();

}