package br.com.base.dao.poc;

import java.util.ArrayList;

public class Main {

	public static ArrayList<String> getCont() {
		ArrayList<String> cont = new ArrayList<String>();
		cont.add("CD-100");
		cont.add("CD-200");
		cont.add("DATAPROM DP-40");
		cont.add("DIGICON FCA");
		cont.add("ESC-1");
		cont.add("ESC-2");
		cont.add("ESC-4");
		cont.add("FLEX-III");
		cont.add("FLEX-III/A");
		cont.add("FLEX-IV");
		cont.add("GREEN WAVE-FX");
		cont.add("GREEN WAVE-TR");
		cont.add("PTC1");
		cont.add("PTC1-5");
		cont.add("RMX-Y3");
		cont.add("RMX-Y4");
		cont.add("RMY-4");
		cont.add("SEC-1");
		cont.add("SEC-2");
		cont.add("SERTTEL");
		cont.add("ST-900");
		cont.add("T-400");
		cont.add("T-800");
		cont.add("T-99");
		cont.add("T99-1");
		cont.add("TRX");
		cont.add("TSC3");
		return cont;
	}

	public static void main(String[] args) {
		String baseQuery = "INSERT INTO controlador(id, endereco_ip, nome) VALUES (nextval(\'controlador_seq\'), \'x.x.x.x\', \'%s\');";
		for (String string : Main.getCont()) {
			System.out.println(String.format(baseQuery, string));
		}
		
	}

}
