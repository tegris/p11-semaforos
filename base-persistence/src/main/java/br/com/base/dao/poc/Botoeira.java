package br.com.base.dao.poc;

import java.util.Set;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Botoeira extends AssociaEstagios {

    /**
     * Serial UID.
     */
    private static final long serialVersionUID = -5181268073274682231L;

    @Override
    @XmlAttribute(name = "id")
    public Long getId() {
        return super.getId();
    }

    @Override
    @XmlTransient    
    public Set<Long> getEstagios() {
        return super.getEstagios();
    }

    @Override
    @XmlAttribute(name = "estagios")
    public String getEstagiosXML() {
        return super.getEstagiosXML();
    }

}
