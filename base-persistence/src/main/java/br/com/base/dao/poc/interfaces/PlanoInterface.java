package br.com.base.dao.poc.interfaces;

import java.io.Serializable;
import java.util.Set;

public interface PlanoInterface extends Serializable {

    public Long getId();

    public Integer getTempoDeCiclo();

    public Integer getTempoDeCicloMaximo();

    public Set<? extends AnelInterface> getAneis();

}