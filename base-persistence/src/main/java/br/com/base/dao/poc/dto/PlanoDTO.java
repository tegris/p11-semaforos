package br.com.base.dao.poc.dto;

import java.io.Serializable;
import java.util.Set;
import br.com.base.dao.poc.interfaces.PlanoInterface;

public class PlanoDTO implements Serializable, PlanoInterface {

    /**
	 * 
	 */
    private static final long serialVersionUID = -5109277388238057915L;
    
    private String nome;

    private Long id;

    private Integer tempoDeCiclo;

    private Integer tempoDeCicloMaximo;

    private Set<AnelDTO> aneis;

    /*
     * (non-Javadoc)
     * 
     * @see br.com.base.dao.poc.dto.PlanoInterface#getId()
     */
    @Override
    public Long getId() {
        return id;
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.base.dao.poc.dto.PlanoInterface#setId(java.lang.Long)
     */
    public void setId(Long id) {
        this.id = id;
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.base.dao.poc.dto.PlanoInterface#getTempoDeCiclo()
     */
    @Override
    public Integer getTempoDeCiclo() {
        return tempoDeCiclo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.base.dao.poc.dto.PlanoInterface#setTempoDeCiclo(java.lang.Integer)
     */
    public void setTempoDeCiclo(Integer tempoDeCiclo) {
        this.tempoDeCiclo = tempoDeCiclo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.base.dao.poc.dto.PlanoInterface#getTempoDeCicloMaximo()
     */
    @Override
    public Integer getTempoDeCicloMaximo() {
        return tempoDeCicloMaximo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.base.dao.poc.dto.PlanoInterface#setTempoDeCicloMaximo(java.lang.Integer)
     */
    public void setTempoDeCicloMaximo(Integer tempoDeCicloMaximo) {
        this.tempoDeCicloMaximo = tempoDeCicloMaximo;
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.base.dao.poc.dto.PlanoInterface#getAneis()
     */
    @Override
    public Set<AnelDTO> getAneis() {
        return aneis;
    }

    public void setAneis(Set<AnelDTO> aneis) {
        this.aneis = aneis;

    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
