package br.com.base.dao.poc;

import br.com.base.dao.BaseDAO;

public class SemaforoDAO extends BaseDAO<Semaforo> {

    @Override
    protected Class<Semaforo> getEntityType() {
        return Semaforo.class;
    }

}
