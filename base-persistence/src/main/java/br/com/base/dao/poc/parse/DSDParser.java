package br.com.base.dao.poc.parse;

import java.io.ByteArrayOutputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import br.org.dsd.common.jpa.model.LongModel;

/**
 * Classe responsável por fazer o marshall de um longModel para um ByteArray. Necessária a utilização da anotação @XmlRootElement no model em questão.
 * @author Gabriel Pinheiro, Felipe Piacsek
 */
public abstract class DSDParser {

    public static <T extends LongModel> ByteArrayOutputStream marshallToByteArray(final T longModel) {
        try {
            JAXBContext jc = JAXBContext.newInstance(longModel.getClass());
            Marshaller u = jc.createMarshaller();
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            u.marshal(longModel, os);
            return os;
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }
}
