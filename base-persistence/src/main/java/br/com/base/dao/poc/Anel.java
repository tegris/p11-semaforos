package br.com.base.dao.poc;

import java.util.Set;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import br.com.base.dao.poc.interfaces.AnelInterface;
import br.org.dsd.common.jpa.model.LongModel;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Anel extends LongModel implements AnelInterface {

    /**
     * Serial UID.
     */
    private static final long serialVersionUID = -5640250425699382654L;

    private Long id;

    private Integer tempoDeDefasagem;

    private Long estagioQueRecebeTempoDeEstagioDispensavel;

    private Set<Estagio> estagios;

    private Configuracao configuracao;

    @Override
    @XmlAttribute
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @XmlAttribute
    public Integer getTempoDeDefasagem() {
        return tempoDeDefasagem;
    }

    public void setTempoDeDefasagem(Integer tempoDeDefasagem) {
        this.tempoDeDefasagem = tempoDeDefasagem;
    }

    @XmlAttribute
    public Long getEstagioQueRecebeTempoDeEstagioDispensavel() {
        return estagioQueRecebeTempoDeEstagioDispensavel;
    }

    public void setEstagioQueRecebeTempoDeEstagioDispensavel(Long estagioQueRecebeTempoDeEstagioDispensavel) {
        this.estagioQueRecebeTempoDeEstagioDispensavel = estagioQueRecebeTempoDeEstagioDispensavel;
    }

    @XmlElementWrapper
    @XmlElements({@XmlElement(name = "estagio")})
    public Set<Estagio> getEstagios() {
        return estagios;
    }

    public void setEstagios(Set<Estagio> estagios) {
        this.estagios = estagios;
    }

    public Configuracao getConfiguracao() {
        return configuracao;
    }

    public void setConfiguracao(Configuracao configuracao) {
        this.configuracao = configuracao;
    }

}
