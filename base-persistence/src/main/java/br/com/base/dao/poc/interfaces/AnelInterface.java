package br.com.base.dao.poc.interfaces;

import java.io.Serializable;
import java.util.Set;
import br.com.base.dao.poc.Estagio;

public interface AnelInterface extends Serializable {

    public Long getId();

    public Integer getTempoDeDefasagem();

    public Long getEstagioQueRecebeTempoDeEstagioDispensavel();

    public Set<Estagio> getEstagios();
    
    public ConfiguracaoInterface getConfiguracao();

}