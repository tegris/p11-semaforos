package br.com.base.dao.filter;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

/**
 * Filtro para múltiplas possibilidades de valores.
 * @author wilerson.oliveira
 */
public class InMultipleFilter implements Filter {

    private Path path;
    private List<Object> inParameters;

    /**
     * {@inheritDoc}
     */
    @Override
    public Predicate buildPredicate() {
        return path.in(inParameters);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InMultipleFilter setCriteriaBuilder(CriteriaBuilder cb) {
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InMultipleFilter setPath(Path path) {
        this.path = path;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InMultipleFilter setConvertedValue(Object value) {
        this.inParameters = (List<Object>) value;
        return this;
    }
}
