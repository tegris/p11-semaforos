package br.com.base.dao.poc.dto;

import java.io.Serializable;
import java.util.Set;
import br.com.base.dao.poc.Configuracao;
import br.com.base.dao.poc.Estagio;
import br.com.base.dao.poc.interfaces.AnelInterface;
import br.com.base.dao.poc.interfaces.ConfiguracaoInterface;

public class AnelDTO implements Serializable, AnelInterface {

    /**
     * Serial UID.
     */
    private static final long serialVersionUID = -4239788752146395622L;

    private Long id;

    private Integer tempoDeDefasagem;

    private Long estagioQueRecebeTempoDeEstagioDispensavel;

    private Set<Estagio> estagios;
    
    private ConfiguracaoDTO configuracao;

    /*
     * (non-Javadoc)
     * 
     * @see br.com.base.dao.poc.dto.AnelInterface#getId()
     */
    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.base.dao.poc.dto.AnelInterface#getTempoDeDefasagem()
     */
    @Override
    public Integer getTempoDeDefasagem() {
        return tempoDeDefasagem;
    }

    public void setTempoDeDefasagem(Integer tempoDeDefasagem) {
        this.tempoDeDefasagem = tempoDeDefasagem;
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.base.dao.poc.dto.AnelInterface# getEstagioQueRecebeTempoDeEstagioDispensavel()
     */
    @Override
    public Long getEstagioQueRecebeTempoDeEstagioDispensavel() {
        return estagioQueRecebeTempoDeEstagioDispensavel;
    }

    public void setEstagioQueRecebeTempoDeEstagioDispensavel(Long estagioQueRecebeTempoDeEstagioDispensavel) {
        this.estagioQueRecebeTempoDeEstagioDispensavel = estagioQueRecebeTempoDeEstagioDispensavel;
    }

    /*
     * (non-Javadoc)
     * 
     * @see br.com.base.dao.poc.dto.AnelInterface#getEstagios()
     */
    @Override
    public Set<Estagio> getEstagios() {
        return estagios;
    }

    public void setEstagios(Set<Estagio> estagios) {
        this.estagios = estagios;
    }

    @Override
    public ConfiguracaoInterface getConfiguracao() {
        return configuracao;
    }

    public void setConfiguracao(ConfiguracaoDTO configuracao) {
        this.configuracao = configuracao;
    }

}
