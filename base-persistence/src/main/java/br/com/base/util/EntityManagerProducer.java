package br.com.base.util;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


public class EntityManagerProducer {

    /**
     * Produz um EntityManager baseado no PersitenceContext.
     */
    @Produces
    @PersistenceContext
    private EntityManager em;

}
