package br.com.base.util;

public interface KeyValueInterface {

    String getKey();

    String getValue();

    default KeyValuePair toKeyValue() {
        return new KeyValuePair(getKey(), getValue());
    }

}