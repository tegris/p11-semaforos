package br.com.base.exception.mapper;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang.NotImplementedException;
import org.jboss.logging.Logger;

/**
 * Mapeamento da exceção NotImplemented.
 * @author raphael.pinheiro
 */
@Provider
public class NotImplementedExceptionMapper implements ExceptionMapper<NotImplementedException> {

    /**
     * Logger do mapper.
     */
    private static final Logger LOG = Logger.getLogger(NotImplementedExceptionMapper.class);

    @Override
    public Response toResponse(final NotImplementedException exception) {
        LOG.error(exception.getMessage(), exception);
        Map<String, List<String>> erros = new HashMap<>();
        erros.put("erros", Collections.singletonList(exception.getMessage()));
        return Response.status(Response.Status.NOT_FOUND).entity(erros).type(MediaType.APPLICATION_JSON).build();
    }

}

