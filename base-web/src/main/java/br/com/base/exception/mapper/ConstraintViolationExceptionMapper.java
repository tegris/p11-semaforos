package br.com.base.exception.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang.StringUtils;
import org.jboss.logging.Logger;

/**
 * Mapeamento da exceção ConstraintViolationException.
 * @author raphael.pinheiro
 */
@Provider
public class ConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {

    /**
     * Logger do mapper.
     */
    private static final Logger LOG = Logger.getLogger(ConstraintViolationExceptionMapper.class);

    @Override
    public Response toResponse(final ConstraintViolationException exception) {
        Set<ConstraintViolation<?>> constraintViolations = exception.getConstraintViolations();

        List<String> violacoes = constraintViolations.stream().map(cv -> {
            String template = "[%s] Property[%s] InvalidValue[%s] Mensagem[%s]";
            String className = cv.getRootBeanClass().getName();
            String property = StringUtils.capitalize(cv.getPropertyPath().toString());
            Object invalidValue = cv.getInvalidValue();
            String message = cv.getMessage();
            return String.format(template, className, property, invalidValue, message);
        }).collect(Collectors.toList());
        LOG.error(violacoes);
        Map<String, List<String>> erros = new HashMap<>();
        erros.put("erros", violacoes);
        return Response.status(Response.Status.BAD_REQUEST).entity(erros).type(MediaType.APPLICATION_JSON).build();
    }

}
