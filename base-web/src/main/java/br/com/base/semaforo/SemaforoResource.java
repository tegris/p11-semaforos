package br.com.base.semaforo;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.jboss.resteasy.spi.InternalServerErrorException;
import com.google.common.collect.Lists;
import br.com.base.BaseResource;
import br.com.base.business.semaforo.SemaforoService;
import br.com.base.dao.Page;
import br.com.base.dao.poc.Semaforo;
import br.com.base.meta.MetaField;

@Path("/semaforo")
public class SemaforoResource extends BaseResource<Semaforo> {

    @Inject
    private SemaforoService service;

    @Override
    protected Page<Semaforo> obtainResources(Map<String, List<String>> queryParameters, Short pageNumber) {
        return service.paginatedList(queryParameters, pageNumber);
    }

    /**
     * Retorna o XML referente a um semáforo
     * @param semaforo Semáforo que estará contido no XML.
     * @return Response contendo o XML do semáforo.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/xml")
    public Response getResourceContent(final Semaforo semaforo) {
        final ByteArrayOutputStream xml = service.parse(semaforo);
        if (xml != null) {
            try {
                Map<String, String> leXml = new HashMap<>();
                leXml.put("xml", xml.toString("UTF-8"));
                return Response.ok(leXml).build();
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        throw new InternalServerErrorException("Erro!");
    }

    @Override
    protected List<MetaField> generateCamposListagem() {
        return Lists.newArrayList(new MetaField("local", "Local", "String"), new MetaField("distrito", "Distrito", "String"), new MetaField("area", "Área", "String"),
                new MetaField("departamento", "Departamento", "String"), new MetaField("controlador", "Controlador", "String"), new MetaField("dcs", "DCS", "String"));
    }
}
