package br.com.base.controlador;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.spi.InternalServerErrorException;

import br.com.base.BaseResource;
import br.com.base.business.controlador.ControladorService;
import br.com.base.dao.Page;
import br.com.base.dao.poc.Controlador;
import br.com.base.dao.poc.dto.ControladorDTO;
import br.com.base.meta.MetaField;

import com.google.common.collect.Lists;

@Path("/controlador")
public class ControladorResource extends BaseResource<Controlador> {

    @Inject
    private ControladorService service;

    @Override
    protected Page<Controlador> obtainResources(Map<String, List<String>> queryParameters, Short pageNumber) {
        return service.paginatedList(queryParameters, pageNumber);
    }

    /**
     * Retorna o XML referente a um controlador.
     * @param controlador Controlador que estará contido no XML.
     * @return Response contendo o XML do controlador.
     * @throws IllegalAccessException 
     * @throws IllegalArgumentException 
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/xml")
    public Response getResourceContent(final ControladorDTO input) throws IllegalArgumentException, IllegalAccessException {
        final ByteArrayOutputStream xml = service.parse(service.parseControladorFromDTO(input));
        if (xml != null) {
            try {
                Map<String, String> leXml = new HashMap<>();
                leXml.put("xml", xml.toString("UTF-8"));
                return Response.ok(leXml).build();
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        throw new InternalServerErrorException("Erro!");
    }

    @Override
    protected List<MetaField> generateCamposListagem() {
        return Lists.newArrayList(new MetaField("nome", "Nome", "String"), new MetaField("enderecoIp", "Endereço IP", "String"));
    }
}
