package br.com.base;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang.NotImplementedException;

import br.com.base.dao.Page;
import br.com.base.meta.Container;
import br.com.base.meta.MetaButton;
import br.com.base.meta.MetaField;
import br.org.dsd.common.jpa.model.LongModel;

/**
 * Classe base para os endpoints. Os métodos que não são direcionados para os endpoints devem ser estendidos nas subclasses. Métodos não estendidos irão lançar HTTP 404 Nas
 * subclasses, o caminho para o recurso deve ser anotado na declaração da classe.
 * @param <T> Qual a classe de modelo usada
 */
public abstract class BaseResource<T extends LongModel> {

    /**
     * Headers injetados a cada request.
     */
    @Context
    private HttpHeaders httpHeaders;

    /**
     * Contexto contendo parâmetros da query string.
     */
    @Context
    protected UriInfo uriInfo;

    /**
     * Retorna a lista de todos os recursos deste tipo do sistema.
     * @return lista de recursos
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listResources() {
        MultivaluedMap<String, String> queryParameters = uriInfo.getQueryParameters();
        Short pagina = obtainPageNumber(queryParameters);
        Response.ResponseBuilder response = Response.ok();
        response.entity(new Container<>(generateCamposListagem(), obtainResources(queryParameters, pagina), generateMetaButtons()));
        return response.build();
    }

    /**
     * Retorna o recurso correspondente ao ID.
     * @param id identificador do recurso
     * @return recurso
     */
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getResource(@PathParam("id") final long id) {
        T entity = obtainResource(id);
        if (entity == null) {
            throw new NotImplementedException();
        }
        Response.ResponseBuilder response = Response.ok();
        response.entity(new Container<>(generateCamposListagem(), entity));
        return response.build();
    }

    /**
     * Realiza a persistência de uma nova instância deste tipo de recurso no banco de dados, e retorna a URL para acessá-la em chamada futura.
     * @param resource recurso a ser persistido
     * @return URL para acessar o recurso.
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createResource(final T resource) {
        prepareForCreate(resource);
        validateResource(resource);
        Long id = persistResource(resource);
        Response.ResponseBuilder response = Response.created(UriBuilder.fromResource(getClass()).path(Long.toString(id)).build());
        return response.build();
    }

    /**
     * Executa as validações-padrão do recurso.
     * @param resource o recurso a ser validado
     */
    protected void validateResource(final T resource) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<T>> constraintViolations = validator.validate(resource);
        if (constraintViolations.size() > 0) {
            throw new ConstraintViolationException(new HashSet<>(constraintViolations));
        }
    }

    /**
     * Prepara o recurso para ser criado na base de dados.
     * @param resource recurso
     */
    protected void prepareForCreate(final T resource) {
    }

    /**
     * Prepara o recurso para ser atualizado na base de dados.
     * @param resource recurso
     */
    protected void prepareForUpdate(final T resource) {
    }

    /**
     * Atualiza uma instância deste tipo de recurso no banco de dados.
     * @param resource recurso a ser atualizado
     * @return 204 No Content, em caso de sucesso
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateResource(final T resource) {
        prepareForUpdate(resource);
        validateResource(resource);
        updateResourceBase(resource);
        Response.ResponseBuilder response = Response.noContent();
        return response.build();
    }

    /**
     * Remove uma instância deste tipo de recurso do banco de dados.
     * @param id identificador do recurso a ser removido
     * @return 204 No Content, em caso de sucesso
     */
    @DELETE
    @Path("{id}")
    public Response deleteResource(@PathParam("id") final long id) {
        removeResource(id);
        Response.ResponseBuilder response = Response.noContent();
        return response.build();
    }

    /**
     * Extrai número de página dos parâmetros da query string.
     * @param queryParameters parâmetros da query string
     * @return número de página usado
     */
    protected Short obtainPageNumber(final Map<String, List<String>> queryParameters) {
        List<String> paginas = queryParameters.remove("pagina");
        Short pagina = 1;
        if (paginas != null && !paginas.isEmpty()) {
            pagina = Short.valueOf(paginas.get(0));
        }
        return pagina;
    }

    /**
     * Lista de recursos para retornar via serviços. As subclasses devem sobrescrever este método de acordo.
     * @param queryParameters parametros da query, para filtros e paginação
     * @param pageNumber número da página desejada
     * @return objeto de pagina, contendo lista de recursos e contagem total
     */
    protected Page<T> obtainResources(final Map<String, List<String>> queryParameters, final Short pageNumber) {
        throw new NotImplementedException();
    }

    /**
     * Recurso correspondente ao ID para serviços. As subclasses devem sobrescrever este método de acordo.
     * @param id identificador do recurso
     * @return recurso
     */
    protected T obtainResource(final Long id) {
        throw new NotImplementedException();
    }

    /**
     * Persiste o recurso no banco de dados. As subclasses devem sobrescrever este método de acordo.
     * @param resource recurso a inserir
     * @return ID do recurso inserido
     */
    protected Long persistResource(final T resource) {
        throw new NotImplementedException();
    }

    /**
     * Atualiza um recurso existente no banco de dados. As subclasses devem sobrescrever este método de acordo.
     * @param resource recurso a atualizar
     */
    protected void updateResourceBase(final T resource) {
        throw new NotImplementedException();
    }

    /**
     * Remove um recurso existente no banco de dados. As subclasses devem sobrescrever este método de acordo.
     * @param id identificador do recurso a remover
     */
    protected void removeResource(final Long id) {
        throw new NotImplementedException();
    }

    /**
     * Gera os campos que aparecerão na listagem. As subclasses devem sobrescrever este método de acordo.
     * @return campos da listagem
     */
    protected List<MetaField> generateCamposListagem() {
        throw new NotImplementedException();
    }

    /**
     * Gera os meta-botões.
     * @return os meta-botões
     */
    protected MetaButton generateMetaButtons() {
        return new MetaButton();
    }

    /**
     * Retorna os parâmetros de query string da request.
     * @return os parâmetros de query string
     */
    protected MultivaluedMap<String, String> getQueryParameters() {
        return uriInfo.getQueryParameters();
    }


}
