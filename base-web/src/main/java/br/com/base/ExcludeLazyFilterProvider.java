package br.com.base;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.codehaus.jackson.map.ser.impl.SimpleFilterProvider;

@Provider
public class ExcludeLazyFilterProvider extends JacksonJaxbJsonProvider {

    /* (non-Javadoc)
     * @see org.codehaus.jackson.jaxrs.JacksonJsonProvider#writeTo(java.lang.Object, java.lang.Class, java.lang.reflect.Type, java.lang.annotation.Annotation[], javax.ws.rs.core.MediaType, javax.ws.rs.core.MultivaluedMap, java.io.OutputStream)
     */
    @Override
    public void writeTo(final Object value, final Class<?> type, final Type genericType, final Annotation[] annotations, final MediaType mediaType, final MultivaluedMap<String, Object> httpHeaders,
            final OutputStream entityStream) throws IOException {
        locateMapper(type, mediaType).setFilters(new SimpleFilterProvider().addFilter(LazyFilter.NAME, new LazyFilter()));
        super.writeTo(value, type, genericType, annotations, mediaType, httpHeaders, entityStream);
    }



}
