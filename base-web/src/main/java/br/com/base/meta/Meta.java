package br.com.base.meta;

import java.io.Serializable;
import java.util.List;

/**
 * Classe para conter meta-informações de recursos REST.
 */
public class Meta implements Serializable {

    /**
     * Serial UID.
     */
    private static final long serialVersionUID = -8454755916047763063L;

    /**
     * Campos que devem aparecer nas listagens do recurso.
     */
    private List<MetaField> camposListagem;

    /**
     * Número de página.
     */
    private Short pagina;

    /**
     * Contagem de registros.
     */
    private Long contagem;

    private String exibeEditar;

    private String exibeVisualizar;

    private String exibeRemover;

    /**
     * Construtor default.
     */
    public Meta() {

    }

    /**
     * Construtor com todos os parâmetros.
     * @param camposListagem campos da listagem
     * @param pagina número da página
     * @param contagem contagem de registros
     */
    public Meta(final List<MetaField> camposListagem, final Short pagina, final Long contagem) {
        this.camposListagem = camposListagem;
        this.pagina = pagina;
        this.contagem = contagem;
    }

    public void setMetaButtons(final MetaButton metaButton) {
        this.exibeEditar = metaButton.getExibeEditar();
        this.exibeRemover = metaButton.getExibeRemover();
        this.exibeVisualizar = metaButton.getExibeVisualizar();
    }

    /**
     * Retorna os campos das listagens do recurso.
     * @return lista com os campos
     */
    public List<MetaField> getCamposListagem() {
        return camposListagem;
    }

    /**
     * Informa os campos das listagens do recurso.
     * @param camposListagem lista de campos
     */
    public void setCamposListagem(final List<MetaField> camposListagem) {
        this.camposListagem = camposListagem;
    }

    /**
     * Retorno o número da página atual.
     * @return a página
     */
    public short getPagina() {
        return pagina;
    }

    /**
     * Informa o número da página atual.
     * @param pagina a página
     */
    public void setPagina(final Short pagina) {
        this.pagina = pagina;
    }

    /**
     * Retorna a contagem de registros desta resposta.
     * @return a contagem de registros
     */
    public long getContagem() {
        return contagem;
    }

    /**
     * Informa a contagem de registros da resposta
     * @param contagem a contagem de registros
     */
    public void setContagem(final Long contagem) {
        this.contagem = contagem;
    }

    public String getExibeEditar() {
        return exibeEditar;
    }

    public void setExibeEditar(final String exibeEditar) {
        this.exibeEditar = exibeEditar;
    }

    public String getExibeVisualizar() {
        return exibeVisualizar;
    }

    public void setExibeVisualizar(final String exibeVisualizar) {
        this.exibeVisualizar = exibeVisualizar;
    }

    public String getExibeRemover() {
        return exibeRemover;
    }

    public void setExibeRemover(final String exibeRemover) {
        this.exibeRemover = exibeRemover;
    }
}
