package br.com.base.business.semaforo;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import br.com.base.business.BaseServiceInterface;
import br.com.base.dao.BaseDAO;
import br.com.base.dao.poc.Semaforo;
import br.com.base.dao.poc.SemaforoDAO;

@Stateless
@LocalBean
public class SemaforoService implements BaseServiceInterface<Semaforo> {

    @Inject
    private SemaforoDAO semaforoDAO;

    @Override
    public BaseDAO<Semaforo> getDAO() {
        return this.semaforoDAO;
    }

}
