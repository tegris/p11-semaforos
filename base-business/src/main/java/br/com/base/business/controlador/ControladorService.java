package br.com.base.business.controlador;

import java.util.HashSet;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import br.com.base.business.BaseServiceInterface;
import br.com.base.dao.BaseDAO;
import br.com.base.dao.poc.Anel;
import br.com.base.dao.poc.Botoeira;
import br.com.base.dao.poc.Configuracao;
import br.com.base.dao.poc.Controlador;
import br.com.base.dao.poc.ControladorDAO;
import br.com.base.dao.poc.DetetorVeiculo;
import br.com.base.dao.poc.Plano;
import br.com.base.dao.poc.SequencialEstagio;
import br.com.base.dao.poc.dto.ControladorDTO;
import br.com.base.dao.poc.interfaces.AnelInterface;
import br.com.base.dao.poc.interfaces.AssociaEstagiosInterface;
import br.com.base.dao.poc.interfaces.PlanoInterface;

@Stateless
@LocalBean
public class ControladorService implements BaseServiceInterface<Controlador> {

    @Inject
    private ControladorDAO controladorDAO;

    @Override
    public BaseDAO<Controlador> getDAO() {
        return this.controladorDAO;
    }

    public Controlador parseControladorFromDTO(ControladorDTO dto) throws IllegalArgumentException, IllegalAccessException {
        Controlador controlador = new Controlador();
        controlador.setTimestamp(dto.getTimestamp());
        controlador.setPlanos(new HashSet<Plano>());
        for (PlanoInterface p : dto.getPlanos()) {
            controlador.getPlanos().add(extractPlano(p));
        }
        return controlador;
    }

    private Plano extractPlano(PlanoInterface p) {
        Plano plano = new Plano();
        plano.setTempoDeCiclo(p.getTempoDeCiclo());
        plano.setTempoDeCicloMaximo(p.getTempoDeCicloMaximo());
        plano.setId(p.getId());
        plano.setAneis(new HashSet<Anel>());
        for (AnelInterface a : p.getAneis()) {
            plano.getAneis().add(extractAnel(a));
        }
        return plano;
    }

    private Anel extractAnel(AnelInterface a) {
        Anel anel = new Anel();
        anel.setId(a.getId());
        anel.setEstagioQueRecebeTempoDeEstagioDispensavel(a.getEstagioQueRecebeTempoDeEstagioDispensavel());
        anel.setTempoDeDefasagem(a.getTempoDeDefasagem());
        anel.setEstagios(a.getEstagios());
        Configuracao configuracao = extractConfiguracao(a);
        anel.setConfiguracao(configuracao);
        return anel;
    }

    private Configuracao extractConfiguracao(AnelInterface a) {
        Configuracao configuracao = new Configuracao();
        configuracao.setEstagiosAssociadosBotoeira(new HashSet<Botoeira>());
        for (AssociaEstagiosInterface bto : a.getConfiguracao().getEstagiosAssociadosBotoeira()) {
            Botoeira botoeira = new Botoeira();
            botoeira.setId(bto.getId());
            botoeira.setEstagios(bto.getEstagios());
            configuracao.getEstagiosAssociadosBotoeira().add(botoeira);
        }                
        configuracao.setEstagiosAssociadosDeteccaoVeiculos(new HashSet<DetetorVeiculo>());
        for (AssociaEstagiosInterface dtv : a.getConfiguracao().getEstagiosAssociadosDeteccaoVeiculos()) {
            DetetorVeiculo detetorVeiculo = new DetetorVeiculo();
            detetorVeiculo.setId(dtv.getId());
            detetorVeiculo.setEstagios(dtv.getEstagios());
            configuracao.getEstagiosAssociadosDeteccaoVeiculos().add(detetorVeiculo);
        }
        configuracao.setSequenciaDeEstagio(new SequencialEstagio());
        return configuracao;
    }

}
