# ** POC Projeto 11 - Central de operação de semáforos de tempo fixo**  #

# Aspectos Técnicos #

## Backend ##

O sistema foi programado em Java EE, utilizando a versão 8.

Possui uma arquitetura desacoplada entre Frontend e Backend, sendo que a comunicação entre ambos é feita usando serviços REST.

O sistema foi projetado para rodar em JBoss EAP 6.4 (http://www.jboss.org/products/eap/download/), e utiliza um banco de dados PostgreSQL 9.4 (http://www.postgresql.org/download/)

Abaixo são apresentadas as principais dependências do sistema:

### Resteasy - Dependência para Webservices rest###

Resteasy é um projeto realizado pela JBoss que oferece diversos frameworks que auxiliam na construção de aplicações Java Restful e serviços Web Restful. Além disso, é uma implementação de JAX-RS, certificada, portátil e específica.  Esta API é aceita como padrão para serviços Web Restful em Java.

Esta dependência foi utilizada para comunicação entre backend e frontend.

### JAXB – Dependência para parsing de XML ###

A arquitetura de Java para XML Binding (JAXB) permite que os desenvolvedores Java mapeiem suas classes afim de definir representações XML para elas. O JABX tem como principal característica a possibilidade de converter os objetos Java em XML e vice-versa. 

### Jackson – Dependência para parsing de JSON ###

Esta tecnologia é utilizada para realizar a conversão de objetos na notação de Javascript (JSON) em objetos Java, e vice e versa. Além disso, seu funcionamento semelhante ao JAXB. 
Utilizada para converter os objetos enviados pelo frontend ao backend.

## Geração do plano como XML ##

O frontend apresenta um formulário web para cadastro de novos planos para controladores de semáforo.

Uma vez que o formulário estiver preenchido, as informações nele contidas serão enviadas ao backend via um Web Service Restful na forma de JSON.

Utilizando o Jackson, o backend converte esse JSON numa estrutura de Objetos Java.

Com os objetos Java em mão, o sistema utiliza um conjunto de interfaces para converter estes objetos que mapeiam o JSON do formulário em outros objetos que, por sua vez, mapeiam o XML desejado.
Isso foi feito para evitar interferência entre as API’s de transformação (JAXB e Jackson), uma vez que essa acarretaria em erros na conversão entre os formatos.

Por fim utiliza-se o JAXB para gerar o XML referente à estrutura de objetos.

O backend responde ao Web Service com o link do arquivo XML gerado.

## Frontend ##

Para o frontend foi desenvolvida uma página web responsiva, ilustrando as principais funcionalidades propostas para a prova de conceito.

Utilizou-se o AngularJS como framework de desenvolvimento da aplicação, com componentes do Bootstrap para elementos visuais, juntamente com uma combinação entre a CSS disponibilizada pelo próprio Bootstrap e classes desenvolvidas especificamente para esta aplicação.

Os componentes de mapa foram desenvolvidos com Leaflet e o OpenStreetMap, como propostos na documentação.

Abaixo são apresentadas com mais detalhes as tecnologias do frontend.

## AngularJS ##

AngularJS é um framework para desenvolvimento de aplicações web, mantido pelo Google que estende o HTML puro, utilizando atributos HTML específicos que são capazes de adicionar um gama de funcionalidades às páginas web.

A parte programática do AngularJS é feita utilizando Javascript.

O AngularJS trabalha lendo o HTML de uma página, procurando por seus atributos e aplicando o comportamento correspondente ao atributo.

O AngularJS é um framework Single-Page-Application (SPA) e Model-View-Controller (MVC) client-side.

## SPA ##

Praticamente todo o código necessário é carregado com um único load, ou seja, todo HTML, JavaScript e CSS são carregados no page load. Outros recursos dinâmicos são carregados quando necessário, geralmente em resposta a ações do usuários.

## Bootstrap ##

Bootstrap é o framework mais popular de HTML, CSS e Javascript para desenvolvimento de aplicações web responsivas.

O Bootstrap é um projeto opensource, que é hospedado, desenvolvido e mantido no GitHub.

Possui um grande gama de componentes HTML prontos, bem como classes de CSS pré-prontas, que facilitam o trabalho de desenvolvimento de novas aplicações.

## Mapa ##

Para o desenvolvimento das funcionalidades que possuem mapas, além do javascript puro, utilizamos algumas bibliotecas que facilitam o entendimento e o desenvolvimento do código. 

### LeafletJs (http://leafletjs.com) ###

Uma biblioteca opensource Javascript. Possui enorme quantidade de ferramentas que agilizam o desenvolvimento das funcionalidades mais utilizadas no mapa, tais como, marcações, popups, navegação, etc. Possui uma excelente documentação e diversos plugins de apoio.

### leaflet-search (https://github.com/stefanocudini/leaflet-search) ###

Em conjunto com o leaflet, esta biblioteca opensource facilita o desenvolvimento do mecanismo de busca no mapa.


### OpenStreetMap (http://www.openstreetmap.org) ###

Também opensource, serve como o repositório de imagens dos 

# Funcionamento do Sistema #

O sistema entregue nessa POC contempla as seguintes funcionalidades: a exibição em mapa dinâmico dos controladores mapeados; uma tela para exibição de dados detalhados de controladores selecionados pelo usuário; uma tela para cadastro de novos estágios nos anéis do plano de um controlador e para exportação do plano construído para formato XML.
O modo de operação deste sistema é explicado a seguir.
Inicialmente, o usuário visualiza um mapa dinâmico contendo todos os controladores mapeados, conforme deixa notar a Figura 1.


![figura1.jpg](https://bitbucket.org/repo/r6yLAM/images/1120603342-figura1.jpg)

**Figura 1 - Mapa dinâmico para exibição de controladores**


Caso se deseje localizar um controlador específico, pode-se recorrer à ferramenta de busca, localizada no canto inferior esquerdo da página, conforme mostra a Figura 2.


![figura2.jpg](https://bitbucket.org/repo/r6yLAM/images/3192569631-figura2.jpg)

**Figura 2 - Mapa dinâmico com busca de controladores**


Uma vez que o controlador buscado tenha sido encontrado, o usuário pode visualizar detalhes deste controlador clicando sobre ele e selecionando o link “Detalhes”. Este link levará a uma tela com dados deste controlador, dentre os quais: nome do controlador, planos do controlador e tabela horária do controlador. Por meio desta tela o usuário poderá ainda cadastrar um novo plano para o controlador selecionado por meio do botão “Novo Plano”. Vide Figura 3.

![figura3.jpg](https://bitbucket.org/repo/r6yLAM/images/4109152965-figura3.jpg)

**Figura 3 - Detalhes do controlador**

O botão de cadastro de plano levará à tela representada na Figura 4. Nesta tela, o usuário poderá:

•	Preencher o nome, o tempo de ciclo e o tempo máximo de ciclo do plano;

•	Selecionar o tempo de defasagem de dado anel e o estágio ao qual o tempo de estágio dispensável se refere;

•	Cadastrar novos estágios para cada um dos anéis do plano;

•	Exportar o plano e sua estrutura para um arquivo XML.

O cadastro de estágios consta da seleção dos seguintes itens:

•	Indispensável: o usuário informa se o estágio é dispensável ou indispensável;

•	Variável: o usuário informa se o estágio é variável ou invariável;

•	Usual: o usuário informa o tempo de verde usual do estágio;

•	Mínimo: o usuário informa o tempo de verde mínimo do estágio;

•	Máximo: o usuário informa o tempo de verde máximo do estágio;

•	Extensão: o usuário informa o tempo de verde de extensão do estágio;

•	Atenção: o usuário informa o tempo de amarelo do estágio;

•	Intermitente: o usuário informa o tempo de vermelho intermitente do estágio:

•	Limpeza: o usuário informa o tempo de vermelho de limpeza do estágio;

•	Botoneiras: o usuário determina o relacionamento com botoneiras para o estágio;

•	Detector de veículos: o usuário determina o relacionamento com detectores de veículos para o estágio.


![figura4.jpg](https://bitbucket.org/repo/r6yLAM/images/2041191509-figura4.jpg)

**Figura 4 - Tela de cadastro de plano**


Uma vez que o usuário tenha finalizado o preenchimento dos dados do plano, poderá exportar os dados do controlador por meio do botão “Salvar”, localizado no canto inferior direito da página. Esta exportação gerará um arquivo do tipo XML com estrutura que segue a seguinte organização:

* Um controlador possui diversos planos;

* Um plano possui tempo de ciclo, tempo de ciclo máximo e diversos anéis;

* Um anel possui tempo de defasagem, a identificação do estágio ao qual se refere o tempo de estágio dispensável e um conjunto de estágios e uma configuração;

* Cada estágio possui uma definição de indispensável ou dispensável, uma definição de variável ou não variável, além de tempo de verde, tempo de verde mínimo, tempo de verde máximo, tempo de verde de extensão, tempo de amarelo, tempo de vermelho intermitente e tempo de vermelho de limpeza;

* Cada configuração possui a definição de relacionamento entre os estágios e as botoeiras, a definição de relacionamento entre os estágios e os detectores de veículos, bem como o sequenciamento dos estágios do plano.dos estágios do plano.